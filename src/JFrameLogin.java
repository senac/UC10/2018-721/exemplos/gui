
import java.awt.FlowLayout;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

public class JFrameLogin {

    private static JFrame jFrame;
    private static List<User> usuarios;

    public static void main(String[] args) {

        loadUsers();

        jFrame = new JFrame("Login");
        jFrame.setBounds(0, 0, 300, 120);
        jFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        //Labels - textos 
        JLabel jLabelUsuario = new JLabel("Usuario:");
        JLabel jLabelSenha = new JLabel("Senha:");

        //textFields - Campos de texto 
        JTextField jTextFieldUsuario = new JTextField(20);
        JTextField jTextFieldSenha = new JTextField(20);

        // button - Botao
        JButton jButton = new JButton("Login");

        //Adicionar na janela 
        jFrame.setLayout(new FlowLayout()); // Definimos o layout
        jFrame.add(jLabelUsuario);
        jFrame.add(jTextFieldUsuario);
        jFrame.add(jLabelSenha);
        jFrame.add(jTextFieldSenha);
        jFrame.add(jButton);
        // Tratar o evento no botao 
        jButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String usuario = jTextFieldUsuario.getText().trim();
                String senha = jTextFieldSenha.getText().trim();
                login(usuario, senha);

            }

        });

        jFrame.setVisible(true);
    }

    private static void login(String usuario, String senha) {

        User user = new User(usuario, senha);

        if (!user.getUsuario().equals("") && !user.getSenha().equals("")) {

            User usuarioProcurado = null;

            for (int i = 0; i < usuarios.size(); i++) {
                if (usuarios.get(i).equals(user)) {
                    usuarioProcurado = usuarios.get(i);
                }
            }

            if (usuarioProcurado != null && usuarioProcurado.getSenha().equals(user.getSenha())) {
                showMessageInformacao("Seja Bem vindo " + usuario + " !");
            } else {
                showMessageErro("Usuario ou senha invalidos!!!!");
            }

        } else {

            showMessageAlerta("Usuario e senha são obrigatorios!.");

        }

    }

    private static void showMessageInformacao(String mensagem) {
        JOptionPane.showMessageDialog(jFrame, mensagem, "Aviso", JOptionPane.INFORMATION_MESSAGE);
    }

    private static void showMessageErro(String mensagem) {
        JOptionPane.showMessageDialog(jFrame, mensagem, "Erro", JOptionPane.ERROR_MESSAGE);
    }

    private static void showMessageAlerta(String mensagem) {
        JOptionPane.showMessageDialog(jFrame, mensagem, "Alerta", JOptionPane.WARNING_MESSAGE);
    }

    private static void loadUsers() {
        User user1 = new User("123", "123");
        User user2 = new User("aaa", "123");
        User user3 = new User("bbb", "123");
        User user4 = new User("ccc", "123");
        User user5 = new User("ddd", "123");
        User user6 = new User("eee", "123");
        User user7 = new User("fff", "123");
        User user8 = new User("jjj", "123");

        usuarios = new ArrayList<>();

        usuarios.add(user1);
        usuarios.add(user2);
        usuarios.add(user3);
        usuarios.add(user4);
        usuarios.add(user5);
        usuarios.add(user6);
        usuarios.add(user7);
        usuarios.add(user8);

    }

}
